# PyPy Bridge Design V2

This is a braindump of my thoughts at this point in time.

Note: For now let us concern ourselves with only "basic" types: numbers,
strings, lists, and None. Later we will allow passing of arbitrary instances
ala Jython.

## Python Functions

Laurie proposed: Functions are defined by a function `pypy_new_func` and are
immediately available in PHP global scope (Note that PHP has no modules,
only namespaces, which are awkward and not really used).

Example:

```
 $f = pypy_new_func("def f(): print('hello')");
 $f(); // <- Behaves like the PHP closure type. PHP has no 1st class funcs.
```

Laurie also suggested that caller scope be available within calls to
`pypy_new_func`, e.g.:

```
  $v = 3;
  $f = pypy_new_func("def f(): print(v)");
  $f() // prints 3
```

But what is really happening here laurie? Is `v` in a sense a global
variable, or does `pypy_new_func` create a closure, encapsulating the value
of `v`?  Having thought a about this, I dislike both -- I think the PHP
scope should not bleed into Python unless passed as a parameter. PHP has so
much global scope it is amazing.

## Python Modules

Python modules can be created using `pypy_new_mod`, e.g.:

```
$m = pypy_new_mod("mymod", "def f(x): return x+1");
```

In python code we should now be able to `import mymod` and use it like a
normal Python module.

Memebers of a module are accesible using the PHP instance dereference
operator, e.g.:

```
$m->f(2) // prints 3
```

This interface is already implemented.

Perhaps we want a `pypy_get_mod` to get at existing modules.

## Calling PHP from Python

To mirror the behaviour of the way that Python functions nest in PHP, all
PHP functions should pollute the Python global namespace?

E.g. in Python code a call to `phpinfo()` is valid?

I am not keen on overlaying the PHP global namespace onto Python. PHP has so
many globals names are likely to clash.

Currently, I have a `php` module which encapsulates the PHP scope, so this
is valid:

```
function hello() {
    echo "foobar";
}
$src = <<<EOD
import php
def f():
    php.hello()
EOD;
$m = pypy_new_mod("m1", $src);
$m->f();
```

I think Laurie will say this is too explicit and would rather:

```
function hello() {
    echo "foobar";
}
$src = <<<EOD
def f():
    hello() // No intermediate php module
EOD;
$m = pypy_new_mod("m1", $src);
$m->f();
```
